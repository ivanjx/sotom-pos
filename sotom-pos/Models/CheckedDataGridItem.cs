﻿using System.ComponentModel;

namespace sotom_pos.Models
{
    public class CheckedDataGridItem<T> : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        bool m_isChecked;

        public bool IsChecked
        {
            get
            {
                return m_isChecked;
            }
            set
            {
                m_isChecked = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsChecked"));
            }
        }

        public T Model { get; set; }


        public CheckedDataGridItem(T model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            return Model.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
