﻿namespace sotom_pos.Models
{
    public class Item
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Stock { get; set; }

        public float Price { get; set; }


        public override bool Equals(object obj)
        {
            Item comp;
            if ((comp = obj as Item) != null)
            {
                return Id == comp.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
