﻿using System;

namespace sotom_pos.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public DateTime TTime { get; set; }

        public float PaidAmount { get; set; }

        public float SubTotal { get; set; }

        public float Cash { get; set; }

        public string Notes { get; set; }


        public override bool Equals(object obj)
        {
            Transaction comp;
            if ((comp = obj as Transaction) != null)
            {
                return Id == comp.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
