﻿namespace sotom_pos.Models
{
    public class Promotion
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public Item ItemInfo { get; set; }

        public bool IsUsed { get; set; }


        public override bool Equals(object obj)
        {
            Promotion comp;
            if ((comp = obj as Promotion) != null)
            {
                return Id == comp.Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
