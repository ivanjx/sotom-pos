﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using sotom_pos.Models;
using sotom_pos.Commons;
using MySql.Data.MySqlClient;

namespace sotom_pos.Views
{
    /// <summary>
    /// Interaction logic for ItemsPage.xaml
    /// </summary>
    public partial class ItemsPage : Page
    {
        bool m_canGoBack;
        MainWindow m_mainwindow;
        ObservableCollection<Item> m_items;
        Item m_itemBeingEdited;


        public ItemsPage(MainWindow mainwindow)
        {
            async void Init()
            {
                m_canGoBack = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    await LoadItemsAsync();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
                }
                finally
                {
                    m_canGoBack = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }

            InitializeComponent();

            m_items = new ObservableCollection<Item>();
            lstItems.ItemsSource = m_items;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;

                ClearGridItem();
                m_items.Clear();

                m_mainwindow.GoBack();
            }
        }

        async Task LoadItemsAsync()
        {
            m_items.Clear();

            // Listing.
            Item[] items = await Loaders.GetItemsAsync();

            for (int i = 0; i < items.Length; ++i)
            {
                m_items.Add(items[i]);
            }
        }

        void ClearGridItem()
        {
            txtName_gridItem.Clear();
            txtPrice_gridItem.Clear();
            txtStock_gridItem.Clear();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridItem.Visibility = Visibility.Visible;
        }

        private async void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Item selitem = lstItems.SelectedItem as Item;

            if (selitem != null)
            {
                bool allowed = await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected item?");

                if (!allowed)
                {
                    return;
                }

                m_canGoBack = false;
                btnRemove.IsEnabled = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    // Deleting.
                    using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
                    {
                        await dbConn.OpenAsync();

                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbConn;
                            cmd.CommandText = "DELETE FROM items WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", selitem.Id);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    // Done.
                    m_items.Remove(selitem);
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to delete selected item.\nDetails:" + ex.Message);
                }
                finally
                {
                    m_canGoBack = true;
                    btnRemove.IsEnabled = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }
            else
            {
                m_mainwindow.ShowMessage("Error", "No item selected.");
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Item selitem = lstItems.SelectedItem as Item;

            if (selitem != null)
            {
                m_itemBeingEdited = selitem;

                // Filling user inputs.
                txtName_gridItem.Text = selitem.Name;
                txtPrice_gridItem.Text = selitem.Price.ToString();
                txtStock_gridItem.Text = selitem.Stock.ToString();

                // Showing the grid.
                gridItem.Visibility = Visibility.Visible;
            }
            else
            {
                m_mainwindow.ShowMessage("Error", "No item selected.");
            }
        }

        private async void btnOK_gridItem_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = false;
            btnOK_gridItem.IsEnabled = false;
            btnCancel_gridItem.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                // Checking user inputs.
                if (string.IsNullOrEmpty(txtName_gridItem.Text))
                {
                    throw new Exception("Name is empty");
                }

                if (string.IsNullOrEmpty(txtStock_gridItem.Text))
                {
                    throw new Exception("Stock is empty");
                }

                if (string.IsNullOrEmpty(txtPrice_gridItem.Text))
                {
                    throw new Exception("Price is empty");
                }

                float price = float.Parse(txtPrice_gridItem.Text);
                int stock = int.Parse(txtStock_gridItem.Text);

                // Saving to the database.
                using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
                {
                    await dbConn.OpenAsync();

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbConn;

                        if (m_itemBeingEdited == null)
                        {
                            // Add new.
                            cmd.CommandText = "INSERT INTO items(name, price, stock) VALUES(@name, @price, @stock)";
                        }
                        else
                        {
                            // Update existing.
                            cmd.CommandText = "UPDATE items SET name = @name, price = @price, stock = @stock WHERE id = @id";
                            cmd.Parameters.AddWithValue("id", m_itemBeingEdited.Id);
                        }

                        cmd.Parameters.AddWithValue("name", txtName_gridItem.Text);
                        cmd.Parameters.AddWithValue("price", price);
                        cmd.Parameters.AddWithValue("stock", stock);

                        await cmd.ExecuteNonQueryAsync();
                    }
                }

                // Updating items list.
                await LoadItemsAsync();

                // Done.
                btnCancel_gridItem_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to save changes.\nDetails: " + ex.Message);
            }
            finally
            {
                m_canGoBack = true;
                btnOK_gridItem.IsEnabled = true;
                btnCancel_gridItem.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridItem_Click(object sender, RoutedEventArgs e)
        {
            ClearGridItem();
            gridItem.Visibility = Visibility.Hidden;
            m_itemBeingEdited = null;
        }
    }
}
