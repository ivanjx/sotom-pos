﻿using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using sotom_pos.Commons;
using sotom_pos.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace sotom_pos.Views
{
    /// <summary>
    /// Interaction logic for PromotionsPage.xaml
    /// </summary>
    public partial class PromotionsPage : Page
    {
        // View models.
        class PromotionViewModel
        {
            public Promotion Model { get; private set; }

            public string Code
            {
                get
                {
                    return Model.Code;
                }
            }

            public string ItemName
            {
                get
                {
                    return Model.ItemInfo.Name;
                }
            }

            public float ItemPrice
            {
                get
                {
                    return Model.ItemInfo.Price;
                }
            }

            public string Status
            {
                get
                {
                    return Model.IsUsed ? "Used" : "Available";
                }
            }


            public PromotionViewModel(Promotion model)
            {
                Model = model;
            }

            public override bool Equals(object obj)
            {
                PromotionViewModel comp;
                if ((comp = obj as PromotionViewModel) != null)
                {
                    return Model.Equals(comp.Model);
                }

                return false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        // Page definition.
        bool m_canGoBack;
        MainWindow m_mainwindow;
        ObservableCollection<CheckedDataGridItem<PromotionViewModel>> m_promotions;
        ObservableCollection<Item> m_items;


        public PromotionsPage(MainWindow mainwindow)
        {
            async void Init()
            {
                m_canGoBack = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    await LoadPromotionsAsync();

                    // Loading items combo box.
                    Item[] items = await Loaders.GetItemsAsync();

                    for (int i = 0; i < items.Length; ++i)
                    {
                        m_items.Add(items[i]);
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
                }
                finally
                {
                    m_canGoBack = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }

            InitializeComponent();

            m_promotions = new ObservableCollection<CheckedDataGridItem<PromotionViewModel>>();
            lstPromotions.ItemsSource = m_promotions;

            m_items = new ObservableCollection<Item>();
            cmbItems_gridAddPromotion.ItemsSource = m_items;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        async Task LoadPromotionsAsync()
        {
            m_promotions.Clear();

            // Listing.
            Promotion[] promotions = await Loaders.GetPromotionsAsync();

            for (int i = 0; i < promotions.Length; ++i)
            {
                m_promotions.Add(new CheckedDataGridItem<PromotionViewModel>(new PromotionViewModel(promotions[i])));
            }
        }

        void ClearGridAddPromotion()
        {
            cmbItems_gridAddPromotion.SelectedItem = null;
            txtCount_gridAddPromotion.Clear();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridAddPromotion.Visibility = Visibility.Visible;
        }

        private async void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = false;
            btnRemove.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                bool allowed = await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to delete selected promotions?");

                if (!allowed)
                {
                    return;
                }

                // Getting selected promotions.
                List<Promotion> selitems = new List<Promotion>();

                for (int i = 0; i < m_promotions.Count; ++i)
                {
                    if (m_promotions[i].IsChecked)
                    {
                        selitems.Add(m_promotions[i].Model.Model);
                    }
                }

                // Starting new transaction.
                using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
                {
                    await dbConn.OpenAsync();

                    using (MySqlTransaction transaction = await dbConn.BeginTransactionAsync())
                    {
                        // Deleting selected promotions.
                        for (int i = 0; i < selitems.Count; ++i)
                        {
                            using (MySqlCommand cmd = new MySqlCommand())
                            {
                                cmd.Connection = dbConn;
                                cmd.Transaction = transaction;
                                cmd.CommandText = "DELETE FROM promotions WHERE id = @id";
                                cmd.Parameters.AddWithValue("id", selitems[i].Id);

                                await cmd.ExecuteNonQueryAsync();
                            }
                        }

                        // Commiting transaction.
                        transaction.Commit();
                    }
                }

                // Reloading promotions list.
                await LoadPromotionsAsync();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to remove selected item.\nDetails: " + ex.Message);
            }
            finally
            {
                m_canGoBack = true;
                btnRemove.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            btnPrint.IsEnabled = false;

            try
            {
                List<Promotion> selitems = new List<Promotion>();

                for (int i = 0; i < m_promotions.Count; ++i)
                {
                    if (m_promotions[i].IsChecked)
                    {
                        selitems.Add(m_promotions[i].Model.Model);
                    }
                }

                if (selitems.Count == 0)
                {
                    throw new Exception("No item selected");
                }

                // Preparing datasource.
                ReportDataSource dsource = new ReportDataSource("PromotionSet", selitems);

                // Showing print window.
                PrintWindow printWindow = new PrintWindow(dsource, "Resources\\ReportTemplates\\PromotionCard.rdlc");
                printWindow.Show();
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to print.\nDetails: " + ex.Message);
            }
            finally
            {
                btnPrint.IsEnabled = true;
            }
        }

        private async void btnOK_gridAddPromotion_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = false;
            btnOK_gridAddPromotion.IsEnabled = false;
            btnCancel_gridAddPromotion.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                Item selitem = cmbItems_gridAddPromotion.SelectedItem as Item;

                if (selitem == null)
                {
                    throw new Exception("Item not selected");
                }

                int count = int.Parse(txtCount_gridAddPromotion.Text);

                // Inserting to the database.
                using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
                {
                    await dbConn.OpenAsync();

                    using (MySqlTransaction transaction = await dbConn.BeginTransactionAsync())
                    {
                        for (int i = 0; i < count; ++i)
                        {
                            try
                            {
                                using (MySqlCommand cmd = new MySqlCommand())
                                {
                                    cmd.Connection = dbConn;
                                    cmd.Transaction = transaction;
                                    cmd.CommandText = "INSERT INTO promotions(code, promotion_item_id, is_used) VALUES(@code, @itemId, 0)";
                                    cmd.Parameters.AddWithValue("code", MyRandom.RandomString(6));
                                    cmd.Parameters.AddWithValue("itemId", selitem.Id);

                                    await cmd.ExecuteNonQueryAsync();
                                }
                            }
                            catch
                            {
                                // Retrying.
                                --i;
                            }
                        }

                        // Commiting transaction.
                        transaction.Commit();
                    }
                }

                // Reloading promotions list.
                await LoadPromotionsAsync();

                // Done.
                btnCancel_gridAddPromotion_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to generate and save promotion codes.\nDetails: " + ex.Message);
            }
            finally
            {
                m_canGoBack = true;
                btnOK_gridAddPromotion.IsEnabled = true;
                btnCancel_gridAddPromotion.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridAddPromotion_Click(object sender, RoutedEventArgs e)
        {
            ClearGridAddPromotion();
            gridAddPromotion.Visibility = Visibility.Hidden;
        }
    }
}
