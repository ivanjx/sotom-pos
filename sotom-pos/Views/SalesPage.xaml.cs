﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Common;
using System.Dynamic;
using System.Windows;
using System.Windows.Controls;
using MySql.Data.MySqlClient;
using sotom_pos.Commons;
using sotom_pos.Models;

namespace sotom_pos.Views
{
    /// <summary>
    /// Interaction logic for SalesPage.xaml
    /// </summary>
    public partial class SalesPage : Page
    {
        // View models.
        class ItemViewModel : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            Item m_selItem;
            int m_qty;
            bool m_isItemPropertiesEditable;

            public List<Item> Items { get; private set; }

            public string PromotionCode { get; set; }

            public bool IsPaid { get; set; }

            public bool IsItemPropertiesEditable
            {
                get
                {
                    return m_isItemPropertiesEditable;
                }
            }

            public Item SelectedItem
            {
                get
                {
                    return m_selItem;
                }
                set
                {
                    m_selItem = value;
                    Quantity = 0; // Reset the quantity;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SelectedItem"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MaxStock"));
                }
            }

            public int MaxStock
            {
                get
                {
                    return m_selItem == null ? 0 : m_selItem.Stock;
                }
            }

            public int Quantity
            {
                get
                {
                    return m_qty;
                }
                set
                {
                    m_qty = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Quantity"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Price"));
                }
            }

            public float Price
            {
                get
                {
                    return SelectedItem == null ? 0 : SelectedItem.Price * Quantity;
                }
            }


            public ItemViewModel(List<Item> items, bool isItemPropertyEditable)
            {
                Items = items;
                m_isItemPropertiesEditable = isItemPropertyEditable;
            }
        }

        // Page definition.
        bool m_canGoBack;
        MainWindow m_mainwindow;
        ObservableCollection<ItemViewModel> m_items;
        List<Item> m_products;
        Transaction m_transaction;


        public SalesPage(MainWindow mainwindow)
        {
            async void Init()
            {
                m_canGoBack = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    m_products = new List<Item>();
                    Item[] items = await Loaders.GetItemsAsync();

                    for (int i = 0; i < items.Length; ++i)
                    {
                        m_products.Add(items[i]);
                    }
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
                }
                finally
                {
                    m_canGoBack = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }

            InitializeComponent();

            m_canGoBack = true;

            m_items = new ObservableCollection<ItemViewModel>();
            lstItems.ItemsSource = m_items;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_items.Clear();
                m_mainwindow.GoBack();
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            m_items.Add(new ItemViewModel(m_products, true));
        }

        private void btnAddPromotion_Click(object sender, RoutedEventArgs e)
        {
            // Showing the grid.
            gridPromotion.Visibility = Visibility.Visible;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            ItemViewModel selitem = lstItems.SelectedItem as ItemViewModel;

            if (selitem != null)
            {
                m_items.Remove(selitem);
            }
        }

        private async void btnOK_gridPromotion_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = false;
            btnOK_gridPromotion.IsEnabled = false;
            btnCancel_gridPromotion.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                // Checking user inputs.
                if (string.IsNullOrEmpty(txtCode_gridPromotion.Text))
                {
                    throw new Exception("Code is empty");
                }

                // Checking code.
                using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
                {
                    await dbConn.OpenAsync();

                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = dbConn;
                        cmd.CommandText = "SELECT promotion_item_id FROM promotions WHERE is_used = 0 AND code = @code";
                        cmd.Parameters.AddWithValue("code", txtCode_gridPromotion.Text);

                        using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (await reader.ReadAsync())
                            {
                                // Getting product info.
                                dynamic filters = new ExpandoObject();
                                filters.id = reader.GetInt32(0);

                                Item promotionItem = (await Loaders.GetItemsAsync(filters))[0];

                                // Adding to the list.
                                m_items.Add(new ItemViewModel(m_products, false)
                                {
                                    SelectedItem = promotionItem,
                                    Quantity = 1,
                                    PromotionCode = txtCode_gridPromotion.Text,
                                    IsPaid = chkIsPaid_gridPromotion.IsChecked.Value
                                });
                            }
                            else
                            {
                                throw new Exception("Not found");
                            }
                        }
                    }
                }

                // Done.
                btnCancel_gridPromotion_Click(null, null);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to apply promotion code.\nDetails: " + ex.Message);
            }
            finally
            {
                m_canGoBack = true;
                btnOK_gridPromotion.IsEnabled = true;
                btnCancel_gridPromotion.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridPromotion_Click(object sender, RoutedEventArgs e)
        {
            txtCode_gridPromotion.Clear();
            gridPromotion.Visibility = Visibility.Hidden;
        }

        private void btnProceed_Click(object sender, RoutedEventArgs e)
        {
            m_transaction = new Transaction();

            // Calculating subtotal.
            for (int i = 0; i < m_items.Count; ++i)
            {
                if (m_items[i].IsPaid)
                {
                    m_transaction.PaidAmount += m_items[i].Price;
                }
                else
                {
                    m_transaction.SubTotal += m_items[i].Price;
                }
            }

            // Changing label.
            txtPaidAmount_gridProceed.Text = m_transaction.PaidAmount.ToString("C2");
            txtSubtotal_gridProceed.Text = m_transaction.SubTotal.ToString("C2");

            // Showing the grid.
            gridProceed.Visibility = Visibility.Visible;
        }

        private void txtCash_gridProceed_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                // Calculating change.
                m_transaction.Cash = float.Parse(txtCash_gridProceed.Text);
                float change = m_transaction.Cash - m_transaction.SubTotal;
                txtChange_gridProceed.Text = change.ToString("C2");
            }
            catch { }
        }

        private async void btnOK_gridProceed_Click(object sender, RoutedEventArgs e)
        {
            m_canGoBack = false;
            btnOK_gridProceed.IsEnabled = false;
            btnCancel_gridProceed.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                // Checking user inputs.
                if (m_transaction == null)
                {
                    throw new Exception("Transaction is null");
                }

                if (string.IsNullOrEmpty(txtCash_gridProceed.Text))
                {
                    throw new Exception("Cash is empty");
                }

                if (m_transaction.SubTotal > m_transaction.Cash)
                {
                    throw new Exception("Cash is less than subtotal");
                }
                
                using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
                {
                    await dbConn.OpenAsync();

                    // Starting transaction.
                    using (MySqlTransaction transaction = await dbConn.BeginTransactionAsync())
                    {
                        // Saving to transactions table.
                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbConn;
                            cmd.Transaction = transaction;
                            cmd.CommandText = "INSERT INTO transactions(t_time, subtotal, cash, notes) VALUES(@ttime, @subtotal, @cash, @notes)";
                            cmd.Parameters.AddWithValue("ttime", DateTime.Now);
                            cmd.Parameters.AddWithValue("subtotal", m_transaction.SubTotal + m_transaction.PaidAmount);
                            cmd.Parameters.AddWithValue("cash", m_transaction.Cash);
                            cmd.Parameters.AddWithValue("notes", txtNotes_gridProceed.Text);
                            await cmd.ExecuteNonQueryAsync();

                            m_transaction.Id = Convert.ToInt32(cmd.LastInsertedId);
                        }
                        
                        // Processing items.
                        for (int i = 0; i < m_items.Count; ++i)
                        {
                            if (m_items[i].SelectedItem == null)
                            {
                                continue;
                            }

                            // Saving to items_transaction table.
                            using (MySqlCommand cmd = new MySqlCommand())
                            {
                                cmd.Connection = dbConn;
                                cmd.Transaction = transaction;
                                cmd.CommandText = "INSERT INTO items_transaction(transaction_id, item_id, qty) VALUES(@transactionId, @itemId, @qty)";
                                cmd.Parameters.AddWithValue("transactionId", m_transaction.Id);
                                cmd.Parameters.AddWithValue("itemId", m_items[i].SelectedItem.Id);
                                cmd.Parameters.AddWithValue("qty", m_items[i].Quantity);
                                await cmd.ExecuteNonQueryAsync();
                            }

                            // Changing stock.
                            using (MySqlCommand cmd = new MySqlCommand())
                            {
                                cmd.Connection = dbConn;
                                cmd.Transaction = transaction;
                                cmd.CommandText = "UPDATE items SET stock = stock - @qty WHERE id = @itemId";
                                cmd.Parameters.AddWithValue("itemId", m_items[i].SelectedItem.Id);
                                cmd.Parameters.AddWithValue("qty", m_items[i].Quantity);
                                await cmd.ExecuteNonQueryAsync();
                            }

                            // Applying promotion code.
                            if (!string.IsNullOrEmpty(m_items[i].PromotionCode))
                            {
                                using (MySqlCommand cmd = new MySqlCommand())
                                {
                                    cmd.Connection = dbConn;
                                    cmd.Transaction = transaction;
                                    cmd.CommandText = "UPDATE promotions SET is_used = 1 WHERE code = @code";
                                    cmd.Parameters.AddWithValue("code", m_items[i].PromotionCode);
                                    await cmd.ExecuteNonQueryAsync();
                                }
                            }
                        }

                        // Commiting transaction.
                        transaction.Commit();
                    }

                    // Done.
                    btnCancel_gridProceed_Click(null, null);
                    m_items.Clear();
                }
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to complete transaction.\nDetails: " + ex.Message);
            }
            finally
            {
                m_canGoBack = true;
                btnOK_gridProceed.IsEnabled = true;
                btnCancel_gridProceed.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }

        private void btnCancel_gridProceed_Click(object sender, RoutedEventArgs e)
        {
            m_transaction = null;
            txtNotes_gridProceed.Clear();
            txtSubtotal_gridProceed.Clear();
            txtCash_gridProceed.Clear();
            txtChange_gridProceed.Clear();
            gridProceed.Visibility = Visibility.Hidden;
        }
    }
}
