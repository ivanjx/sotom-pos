﻿using MySql.Data.MySqlClient;
using sotom_pos.Commons;
using System;
using System.Windows;
using System.Windows.Controls;

namespace sotom_pos.Views
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        MainWindow m_mainwindow;


        public LoginPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_mainwindow = mainwindow;
        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            StaticData.DatabaseConnection = null;
            btnLogin.IsEnabled = false;
            m_mainwindow.PlayLoadingAnimation();

            try
            {
                // Checking user inputs.
                if (string.IsNullOrEmpty(txtDbHost.Text))
                {
                    throw new Exception("Database host is empty");
                }

                if (string.IsNullOrEmpty(txtDbUsername.Text))
                {
                    throw new Exception("Username is empty");
                }

                if (string.IsNullOrEmpty(txtDbPassword.Password))
                {
                    throw new Exception("Password is empty");
                }

                MySqlConnection dbConn = new MySqlConnection(string.Format("Server={0};Database=db_sotom_pos;User ID={1};Password={2};", txtDbHost.Text, txtDbUsername.Text, txtDbPassword.Password));

                // Testing connection.
                await dbConn.OpenAsync();
                await dbConn.CloseAsync();

                // Success.
                StaticData.DatabaseConnection = dbConn;

                // Clearing user inputs.
                txtDbHost.Clear();
                txtDbUsername.Clear();
                txtDbPassword.Clear();

                // Redirecting to mainpage.
                MainPage mainPage = new MainPage(m_mainwindow);
                m_mainwindow.Navigate(mainPage);
            }
            catch (Exception ex)
            {
                m_mainwindow.ShowMessage("Error", "Unable to login.\nDetails: " + ex.Message);
            }
            finally
            {
                btnLogin.IsEnabled = true;
                m_mainwindow.StopLoadingAnimation();
            }
        }
    }
}
