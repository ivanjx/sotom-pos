﻿using MahApps.Metro.Controls;
using Microsoft.Reporting.WinForms;

namespace sotom_pos.Views
{
    /// <summary>
    /// Interaction logic for PrintWindow.xaml
    /// </summary>
    public partial class PrintWindow : MetroWindow
    {
        public PrintWindow(ReportDataSource dsource, string templatePath)
        {
            InitializeComponent();

            rv.LocalReport.DataSources.Add(dsource);
            rv.LocalReport.ReportPath = templatePath;
            rv.RefreshReport();
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            rv.Dispose();
        }
    }
}
