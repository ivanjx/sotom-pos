﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace sotom_pos.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public delegate void BackRequestedHandler(Page content);
        public event BackRequestedHandler BackRequested;


        public MainWindow()
        {
            InitializeComponent();

            // Setting up language.
            Thread.CurrentThread.CurrentCulture = new CultureInfo("id-ID");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("id-ID");

            FrameworkElement.LanguageProperty.OverrideMetadata(
              typeof(FrameworkElement),
              new FrameworkPropertyMetadata(
                    XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            // Disabling backspace actions.
            NavigationCommands.BrowseBack.InputGestures.Clear();
            NavigationCommands.BrowseForward.InputGestures.Clear();

            // Navigating to login page.
            LoginPage loginPage = new LoginPage(this);
            Navigate(loginPage);
        }

        public async void ShowMessage(string title, string message)
        {
            await Task.Delay(100);
            await this.ShowMessageAsync(title, message);
        }

        public async Task<bool> ShowPromptAsync(string title, string message)
        {
            await Task.Delay(100);
            MessageDialogResult dres = await this.ShowMessageAsync(title, message, MessageDialogStyle.AffirmativeAndNegative);
            return dres == MessageDialogResult.Affirmative;
        }

        public void PlayLoadingAnimation()
        {
            pringLoading.IsActive = true;
        }

        public void StopLoadingAnimation()
        {
            pringLoading.IsActive = false;
        }

        public void Navigate(Page page)
        {
            frmMain.Navigate(page);
        }

        public void GoBack()
        {
            if (frmMain.CanGoBack)
            {
                frmMain.GoBack();
            }
        }

        void ShowBackButton()
        {
            LeftWindowCommands.Width = btnBack.Width;
        }

        void HideBackButton()
        {
            LeftWindowCommands.Width = 0;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            BackRequested?.Invoke(frmMain.Content as Page);
        }

        private void Frame_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Page content = frmMain.Content as Page;
            this.Title = string.Format("{0} - SOTOM POS", content.Title);

            if (frmMain.CanGoBack)
            {
                ShowBackButton();
            }
            else
            {
                HideBackButton();
            }
        }
    }
}
