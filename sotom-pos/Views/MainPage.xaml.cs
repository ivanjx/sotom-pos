﻿using System.Windows;
using System.Windows.Controls;

namespace sotom_pos.Views
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        MainWindow m_mainwindow;
        bool m_loggingOut;


        public MainPage(MainWindow mainwindow)
        {
            InitializeComponent();

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;
        }

        private async void M_mainwindow_BackRequested(Page content)
        {
            if (content == this && !m_loggingOut)
            {
                m_loggingOut = true;
                bool allowed = await m_mainwindow.ShowPromptAsync("Confirmation", "Are you sure want to logout?");

                if (!allowed)
                {
                    m_loggingOut = false;
                    return;
                }

                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_mainwindow.GoBack();
            }
        }

        private void btnItems_Click(object sender, RoutedEventArgs e)
        {
            ItemsPage p = new ItemsPage(m_mainwindow);
            m_mainwindow.Navigate(p);
        }

        private void btnSales_Click(object sender, RoutedEventArgs e)
        {
            SalesPage p = new SalesPage(m_mainwindow);
            m_mainwindow.Navigate(p);
        }

        private void btnTransactions_Click(object sender, RoutedEventArgs e)
        {
            TransactionsPage p = new TransactionsPage(m_mainwindow);
            m_mainwindow.Navigate(p);
        }

        private void btnPromotions_Click(object sender, RoutedEventArgs e)
        {
            PromotionsPage p = new PromotionsPage(m_mainwindow);
            m_mainwindow.Navigate(p);
        }
    }
}
