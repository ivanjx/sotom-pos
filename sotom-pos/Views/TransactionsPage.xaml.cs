﻿using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using sotom_pos.Commons;
using sotom_pos.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace sotom_pos.Views
{
    /// <summary>
    /// Interaction logic for TransactionsPage.xaml
    /// </summary>
    public partial class TransactionsPage : Page
    {
        bool m_canGoBack;
        MainWindow m_mainwindow;
        ObservableCollection<Transaction> m_transactions;
        ObservableCollection<TransactionItem> m_transactionItems;


        public TransactionsPage(MainWindow mainwindow)
        {
            async void Init()
            {
                m_canGoBack = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    await LoadTransactionsAsync();
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to initialize.\nDetails: " + ex.Message);
                }
                finally
                {
                    m_canGoBack = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }

            InitializeComponent();

            m_transactions = new ObservableCollection<Transaction>();
            lstTransactions.ItemsSource = m_transactions;

            m_transactionItems = new ObservableCollection<TransactionItem>();
            lstItems_gridTransactionDetails.ItemsSource = m_transactionItems;

            m_mainwindow = mainwindow;
            m_mainwindow.BackRequested += M_mainwindow_BackRequested;

            Init();
        }

        private void M_mainwindow_BackRequested(Page content)
        {
            if (content == this && m_canGoBack)
            {
                m_mainwindow.BackRequested -= M_mainwindow_BackRequested;
                m_transactions.Clear();
                m_transactionItems.Clear();
                m_mainwindow.GoBack();
            }
        }

        async Task LoadTransactionsAsync()
        {
            m_transactions.Clear();

            // Listing.
            Transaction[] transactions = await Loaders.GetTransactionsAsync();
            float total = 0;

            for (int i = 0; i < transactions.Length; ++i)
            {
                m_transactions.Add(transactions[i]);
                total += transactions[i].SubTotal;
            }

            // Displaying total.
            lblTotal.Text = string.Format("Total earnings:\n{0}", total.ToString("C2"));
        }

        private void btnPrint_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Adding to data source.
            ReportDataSource dsource = new ReportDataSource("TransactionSet", m_transactions);

            // Showing print dialog.
            PrintWindow printWindow = new PrintWindow(dsource, "Resources\\ReportTemplates\\TransactionsNote.rdlc");
            printWindow.Show();
        }

        private async void btnTransactionDetails_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Transaction selitem = lstTransactions.SelectedItem as Transaction;

            if (selitem != null)
            {
                btnTransactionDetails.IsEnabled = false;
                m_mainwindow.PlayLoadingAnimation();

                try
                {
                    // Filling user inputs.
                    txtTime_gridTransactionDetails.Text = selitem.TTime.ToString("yyyy-MM-dd HH:mm:ss");
                    txtSubtotal_gridTransactionDetails.Text = selitem.SubTotal.ToString("C2");
                    txtNotes_gridTransactionDetails.Text = selitem.Notes;

                    // Loading items.
                    using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
                    {
                        await dbConn.OpenAsync();

                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            cmd.Connection = dbConn;
                            cmd.CommandText = "SELECT i.name, i.price, it.qty FROM items_transaction it, items i WHERE it.transaction_id = @transactionId AND it.item_id = i.id";
                            cmd.Parameters.AddWithValue("transactionId", selitem.Id);

                            using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                            {
                                while (await reader.ReadAsync())
                                {
                                    TransactionItem ti = new TransactionItem();
                                    ti.Name = reader.GetString(0);
                                    ti.Price = reader.GetFloat(1);
                                    ti.Quantity = reader.GetInt32(2);

                                    m_transactionItems.Add(ti);
                                }
                            }
                        }
                    }

                    // Showing the grid.
                    gridTransactionDetails.Visibility = System.Windows.Visibility.Visible;
                }
                catch (Exception ex)
                {
                    m_mainwindow.ShowMessage("Error", "Unable to load transaction details.\nDetails: " + ex.Message);
                }
                finally
                {
                    btnTransactionDetails.IsEnabled = true;
                    m_mainwindow.StopLoadingAnimation();
                }
            }
            else
            {
                m_mainwindow.ShowMessage("Error", "No item selected.");
            }
        }

        private void btnOK_gridTransactionDetails_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Clearing user inputs.
            txtNotes_gridTransactionDetails.Clear();
            txtSubtotal_gridTransactionDetails.Clear();
            txtTime_gridTransactionDetails.Clear();
            m_transactionItems.Clear();

            // Hiding the grid.
            gridTransactionDetails.Visibility = System.Windows.Visibility.Hidden;
        }
    }
}
