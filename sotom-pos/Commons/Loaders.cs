﻿using System.Collections.Generic;
using System.Data.Common;
using System.Dynamic;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using sotom_pos.Models;

namespace sotom_pos.Commons
{
    public static class Loaders
    {
        public static async Task<Item[]> GetItemsAsync(dynamic filters = null)
        {
            using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
            {
                await dbConn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbConn;
                    cmd.CommandText = "SELECT * FROM items";

                    if (filters != null)
                    {
                        cmd.CommandText += " WHERE ";

                        if (filters.id != null)
                        {
                            cmd.CommandText += "id = @id";
                            cmd.Parameters.AddWithValue("id", filters.id);
                        }
                    }

                    List<Item> results = new List<Item>();

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Item item = new Item();
                            item.Id = reader.GetInt32(0);
                            item.Name = reader.GetString(1);
                            item.Price = reader.GetFloat(2);
                            item.Stock = reader.GetInt32(3);

                            results.Add(item);
                        }
                    }

                    return results.ToArray();
                }
            }
        }

        public static async Task<Promotion[]> GetPromotionsAsync(dynamic filters = null)
        {
            using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
            {
                await dbConn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbConn;
                    cmd.CommandText = "SELECT * FROM promotions";

                    if (filters != null)
                    {
                        cmd.CommandText += " WHERE ";
                        int count = 0;

                        if (filters.code != null)
                        {
                            cmd.CommandText += "code = @code";
                            cmd.Parameters.AddWithValue("code", filters.code);

                            ++count;
                        }

                        if (filters.isUsed != null)
                        {
                            if (count > 0)
                            {
                                cmd.CommandText += " AND ";
                            }

                            if (filters.isUsed)
                            {
                                cmd.CommandText += "is_used = 1";
                            }
                            else
                            {
                                cmd.CommandText += "is_used = 0";
                            }
                        }
                    }

                    List<Promotion> results = new List<Promotion>();

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            Promotion promotion = new Promotion();
                            promotion.Id = reader.GetInt32(0);
                            promotion.Code = reader.GetString(1);

                            // Getting item info.
                            dynamic itemFilter = new ExpandoObject();
                            itemFilter.id = reader.GetInt32(2);
                            promotion.ItemInfo = (await GetItemsAsync(itemFilter))[0];

                            promotion.IsUsed = reader.GetBoolean(3);

                            // Done.
                            results.Add(promotion);
                        }
                    }

                    return results.ToArray();
                }
            }
        }

        public static async Task<Transaction[]> GetTransactionsAsync()
        {
            using (MySqlConnection dbConn = StaticData.DatabaseConnection.Clone() as MySqlConnection)
            {
                await dbConn.OpenAsync();

                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = dbConn;
                    cmd.CommandText = "SELECT * FROM transactions";

                    using (DbDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        List<Transaction> transactions = new List<Transaction>();

                        while (await reader.ReadAsync())
                        {
                            Transaction t = new Transaction();
                            t.Id = reader.GetInt32(0);
                            t.TTime = reader.GetDateTime(1);
                            t.SubTotal = reader.GetFloat(2);
                            t.Cash = reader.GetFloat(3);
                            t.Notes = reader.GetString(4);

                            transactions.Add(t);
                        }

                        return transactions.ToArray();
                    }
                }
            }
        }
    }
}
