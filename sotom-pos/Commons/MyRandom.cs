﻿using System.Text;

namespace sotom_pos.Commons
{
    public static class MyRandom
    {
        static string STR_RAW = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        public static string RandomString(int length)
        {
            StringBuilder sb = new StringBuilder();
            System.Random rnd = new System.Random();

            for (int i = 0; i < length; ++i)
            {
                int index = rnd.Next(0, STR_RAW.Length);
                sb.Append(STR_RAW[index]);
            }

            return sb.ToString();
        }
    }
}
