﻿-- Dropping existing database.
DROP DATABASE IF EXISTS db_sotom_pos;

-- Creating database.
CREATE DATABASE db_sotom_pos;

-- Creating items table.
CREATE TABLE items
(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    price DOUBLE NOT NULL,
    stock INT NOT NULL,
    PRIMARY KEY(id)
);

-- Creating transactions table.
CREATE TABLE transactions
(
    id INT NOT NULL AUTO_INCREMENT,
    t_time DATETIME NOT NULL,
    subtotal DOUBLE NOT NULL,
    cash DOUBLE NOT NULL,
    notes TEXT,
    PRIMARY KEY(id)
);

-- Creating items_transactions table.
CREATE TABLE items_transaction
(
    transaction_id INT NOT NULL,
    item_id INT NOT NULL,
    qty INT NOT NULL,
    FOREIGN KEY(transaction_id) REFERENCES transactions(id) ON DELETE CASCADE,
    FOREIGN KEY(item_id) REFERENCES items(id) ON DELETE CASCADE
);

-- Creating promotions table.
CREATE TABLE promotions
(
    id INT NOT NULL AUTO_INCREMENT,
    code VARCHAR(6) NOT NULL,
    promotion_item_id INT NOT NULL,
    is_used BIT(1) NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(promotion_item_id) REFERENCES items(id) ON DELETE CASCADE,
    UNIQUE(code)
);